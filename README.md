# NASA

### NASA Pic of the day iOS App
---
Use Xcode 9.3 to build and run the App on Master Branch 

Demo Branch can be used for Xcode versions lower than 9.3

TODO:

* Better test cases to MOCK the response
* Better error handling to show different error message to the user rather than Generic Message

App Screenshots

* https://www.imageupload.co.uk/image/EUKj
* https://www.imageupload.co.uk/image/EUKm
* https://www.imageupload.co.uk/image/EUK7
* https://www.imageupload.co.uk/image/EUK9