//
//  DailyPicViewControllerTests.swift
//  NASADailyTests
//
//  Created by jay on 2018-04-13.
//

import XCTest
@testable import NASADaily

class DailyPicViewControllerTests: XCTestCase {
    
    var sut: DailyPicViewController!
    var presenter: MockDailyPicViewPresenter!
    
    struct Constants {
        static let storyBoardName = "Main"
        static let viewControllerID = "DailyPicViewController"
    }
    
    override func setUp() {
        super.setUp()
        let storyBoard = UIStoryboard(name: Constants.storyBoardName, bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: Constants.viewControllerID)
        sut = viewController as! DailyPicViewController
        presenter = MockDailyPicViewPresenter()
        sut.presenter = presenter
        presenter.view = sut
    }
    
    override func tearDown() {
        presenter = nil
        sut = nil
        super.tearDown()
    }
}

extension DailyPicViewControllerTests {
    
    //MARK: Test cases
    func testViewController_is_NotNil() {
        XCTAssertNotNil(sut, "View Controller should NOT be Nil")
    }
    
    func testViewDidLoad_Invokes_Event_In_Presenter() {
        presenter.viewLoad()
        XCTAssertTrue(presenter.viewLoadInvoked, "viewLoad was NOT invoked")
    }
    
    func testPresenterView_Is_DailyPicViewController() {
        XCTAssertTrue(presenter.view is DailyPicViewController, "view is NOT DailyPicViewController")
    }
}

class MockDailyPicViewPresenter: DailyPicViewPresenter {
    var viewLoadInvoked = false
    
    override func viewLoad() {
        viewLoadInvoked = true
    }
}


