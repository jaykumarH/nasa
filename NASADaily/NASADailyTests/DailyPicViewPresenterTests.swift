//
//  NASADailyTests.swift
//  NASADailyTests
//
//  Created by jay on 2018-04-12.
//

import XCTest
@testable import NASADaily

class DailyPicViewPresenterTests: XCTestCase {
    
    var sut: DailyPicViewPresenter!
    var view: MockDailyPicView!
    
    override func setUp() {
        super.setUp()
        sut = DailyPicViewPresenter()
        view = MockDailyPicView()
        sut.view = view
    }
    
    override func tearDown() {
        view = nil
        sut = nil
        super.tearDown()
    }
}

extension DailyPicViewPresenterTests {
    
    //MARK: Test cases
    func testPresenterNotNil() {
        XCTAssertNotNil(sut, "Presenter must NOT be nil")
    }
    
    func testSetupViewInvoked_On_ViewLoad() {
        sut.viewLoad()
        XCTAssertTrue(view.setupViewInvoked," Setup view must be invoked")
    }
    
    func testShowSpinnerInvoked_On_ViewLoad() {
        sut.viewLoad()
        XCTAssertTrue(view.showSpinnerInvoked," Spinner must be invoked")
    }
    
    //TODO: Further test cases could be done by Mocking Network
}

class MockDailyPicView: DailyPicView {
    var showSpinnerInvoked = false
    var hideSpinnerInvoked = false
    var showTitleInvoked = false
    var showImageInvoked = false
    var setupViewInvoked = false
    var showErrorInvoked = false
    
    //MARK: DailyPicView
    func showSpinner() {
        showSpinnerInvoked = true
    }
    
    func hideSpinner() {
        hideSpinnerInvoked = true
    }
    
    func setupView() {
        setupViewInvoked = true
    }
    
    func showTitle(title: String) {
        showTitleInvoked = true
    }
    
    func showError(title: String, message: String) {
        showErrorInvoked = true
    }
    
    func showImage(image: UIImage?) {
        showImageInvoked = true
    }
}


