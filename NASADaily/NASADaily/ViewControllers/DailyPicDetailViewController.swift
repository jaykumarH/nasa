//
//  DailyPicDetailViewController.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import UIKit

class DailyPicDetailViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var fullScreenImageView: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    
    //MARK: Properties
    var image: UIImage?
    
    //MARK: Life cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        fullScreenImageView.image = image
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeButtonClick(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
