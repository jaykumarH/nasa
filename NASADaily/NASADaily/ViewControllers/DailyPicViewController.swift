//
//  ViewController.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import UIKit

class DailyPicViewController: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var picOfDayImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    //MARK: Constants
    struct Constants {
        static let title = "PicoDay"
        static let dailyPicDetailViewControllerID = "DailyPicDetailViewController"
    }
    
    //MARK: Properties
    var presenter: DailyPicViewPresenter!
    let transition = CustomAnimator()
    
    
    //MARK: Life cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = DailyPicViewPresenter()
        presenter.view = self
        presenter.viewLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension DailyPicViewController {
    
    //MARK: Custom Methods
    @objc private func handleImageTap(sender: UITapGestureRecognizer) {
        
        guard let imageView = sender.view as? UIImageView,
            let dailyPicDetailViewController = storyboard?.instantiateViewController(withIdentifier: Constants.dailyPicDetailViewControllerID) as? DailyPicDetailViewController else {
                return
        }
        dailyPicDetailViewController.image = imageView.image
        dailyPicDetailViewController.transitioningDelegate = self
        present(dailyPicDetailViewController, animated: true, completion: nil)
    }
}

extension DailyPicViewController: DailyPicView {
    
    //MARK: DailyPicView
    func showSpinner() {
        showLoader()
    }
    
    func hideSpinner() {
        hideLoader()
    }
    
    func setupView() {
        title = Constants.title
        transition.dismissCompletion = {
            self.titleLabel.isHidden = false
            self.titleLabel.fadeIn()
        }
    }
    
    func showTitle(title: String) {
        titleLabel.text = title
    }
    
    func showError(title: String, message: String) {
        presentAlert(with: title, message: message)
    }
    
    func showImage(image: UIImage?) {
        picOfDayImageView.image = image
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleImageTap(sender:)))
        picOfDayImageView.addGestureRecognizer(tapGesture)
    }
}

extension DailyPicViewController: UIViewControllerTransitioningDelegate {
    
    //MARK: UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.originFrame = picOfDayImageView.superview!.convert(picOfDayImageView.frame, to: nil)
        transition.presenting = true
        titleLabel.fadeOut()
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.presenting = false
        return transition
    }
}

