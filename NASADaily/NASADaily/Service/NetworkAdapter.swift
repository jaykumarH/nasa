//
//  NetworkAdapter.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import UIKit
struct CustomError: Error {
    var title: String = "Error"
    var message: String = "Something went wrong"
}

class NetworkAdapter {
    
    typealias ApodResponseHandler = (_ response: ApodResponse?, _ error: Error?) -> Void
    typealias ImageResponseHandler = (_ image: UIImage?) -> Void
    
    struct Constants {
        static let baseUrl = "https://api.nasa.gov/planetary/apod?"
        static let apiKey = "n2RxnVbTvKBjnCEtxKTeCJafp1pLU0MBCbaLaoGk"
        static let apiKeyParameter = "api_key"
        static let warningTitle = "Warning"
        static let notConnectedMessage = "Please Check your Internet Connection!"
    }
    
    /*
     This method fetches the Pic of The Day json Data
     */
    class func getApodData(completion: @escaping ApodResponseHandler) {
        guard Reachability.isConnectedToNetwork() else {
            completion(nil, CustomError(title: Constants.warningTitle, message: Constants.notConnectedMessage))
            return
        }
        guard let url = URL(string: "\(Constants.baseUrl)\(Constants.apiKeyParameter)=\(Constants.apiKey)") else {
            completion(nil, CustomError())
            return
        }
        let task = URLSession.shared.dataTask(with: url) {(data, response, error ) in
            guard let httpResponse = response as? HTTPURLResponse,
                httpResponse.statusCode == 200 || httpResponse.statusCode == 202 else {
                    completion(nil, CustomError())
                    return
            }
            guard let data = data,
                let responseString = String(data: data, encoding: .utf8),
                let model = ApodResponse(jsonString: responseString), model.code != 400 else {
                    completion(nil, CustomError())
                    return
            }
            // Valid Data
            completion(model, nil)
        }
        task.resume()
    }
    
    /*
     This method fetches the Image from the url received from the APOD Response
     */
    class func getImageData(with url: String, completionHandler: @escaping ImageResponseHandler) {
        guard let url = URL(string: url)  else {
            completionHandler(nil)
            return
        }
        let task = URLSession.shared.dataTask(with: url) {(data, response, error ) in
            guard error == nil else {
                completionHandler(nil)
                return
            }
            
            guard let data = data,
                let image = UIImage(data: data) else {
                    completionHandler(nil)
                    return
            }
            // Valid Image
            completionHandler(image)
        }
        task.resume()
    }
    
}
