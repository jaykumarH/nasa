//
//  ApodResponse.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import Foundation

struct ApodResponse: Mappable {
    let code: Int?
    let errorMessage: String?
    let date: String
    let title: String
    let url: String
    let hdUrl: String
    
    private enum CodingKeys : String, CodingKey {
        case code
        case errorMessage = "msg"
        case date
        case title
        case url
        case hdUrl = "hdurl"
    }
}
