//
//  CustomAnimator.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import Foundation
import UIKit

class CustomAnimator : NSObject, UIViewControllerAnimatedTransitioning {
    
    struct Constants {
        static let duration = 1.0
        static let animationDelay = 0.35
        static let springDamping: CGFloat = 0.4
        static let sprintVelocity:CGFloat = 0.0
    }
    var presenting = true
    var originFrame = CGRect.zero
    var dismissCompletion: (()->Void)?
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Constants.duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        guard let toView = transitionContext.view(forKey: .to),
            let detailView = presenting ? toView : transitionContext.view(forKey: .from) else {
                return
        }
        
        let initialFrame = presenting ? originFrame : detailView.frame
        let finalFrame = presenting ? detailView.frame : originFrame
        
        let xScaleFactor = presenting ? initialFrame.width / finalFrame.width : finalFrame.width / initialFrame.width
        let yScaleFactor = presenting ? initialFrame.height / finalFrame.height : finalFrame.height / initialFrame.height
        let scaleTransform = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)
        
        if presenting {
            detailView.transform = scaleTransform
            detailView.center = CGPoint(x: initialFrame.midX, y: initialFrame.midY)
            detailView.clipsToBounds = true
        }
        
        containerView.addSubview(toView)
        containerView.bringSubview(toFront: detailView)
        
        UIView.animate(withDuration: Constants.duration, delay:Constants.animationDelay,
                       usingSpringWithDamping: Constants.springDamping, initialSpringVelocity: Constants.sprintVelocity,
                       animations: {
                        detailView.transform = self.presenting ? CGAffineTransform.identity : scaleTransform
                        detailView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
        },
                       completion: { _ in
                        if !self.presenting {
                            self.dismissCompletion?()
                        }
                        transitionContext.completeTransition(true)
        })
    }
}
