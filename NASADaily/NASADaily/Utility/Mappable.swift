//
//  Mappable.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import Foundation

protocol Mappable: Decodable {
    init?(jsonString: String)
}
extension Mappable {
    init?(jsonString: String) {
        guard let data = jsonString.data(using: .utf8), data.count > 0,
            let model = try? JSONDecoder().decode(Self.self, from: data) else {
                return nil
        }
        self = model
    }
}
