//
//  DailyPicViewPresenter.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//
import UIKit

protocol DailyPicView: class {
    func showSpinner()
    func hideSpinner()
    func showTitle(title: String)
    func showImage(image: UIImage?)
    func setupView()
    func showError(title: String, message: String)
}

class DailyPicViewPresenter {
    
    weak var view: DailyPicView?
    struct Constants {
        static let defaultErrorImageName = "placeholder"
    }
    
    func viewLoad() {
        view?.setupView()
        view?.showSpinner()
        NetworkAdapter.getApodData { [weak self] (model, error) in
            guard let error = error as? CustomError else {
                if let model = model {
                    NetworkAdapter.getImageData(with: model.url, completionHandler: { [weak self] (image) in
                        self?.view?.hideSpinner()
                        DispatchQueue.main.async {
                            if let image = image {
                                self?.view?.showImage(image: image)
                            } else {
                                self?.view?.showImage(image: self?.getDefaultErrorImage())
                            }
                        }
                    })
                    DispatchQueue.main.async {
                        self?.view?.showTitle(title: model.title)
                    }
                }
                return
            }
            self?.view?.hideSpinner()
            DispatchQueue.main.async {
                self?.view?.showError(title: error.title, message: error.message)
            }
            
        }
    }
    
    private func getDefaultErrorImage() -> UIImage? {
        return UIImage(named: Constants.defaultErrorImageName)
    }
}
