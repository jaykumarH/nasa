//
//  UIViewController.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import Foundation
import UIKit

extension UIViewController {
    
    /*
     This method renders the Spinner view
     */
    func showLoader() {
        let spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 90, height:90))
        spinner.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        spinner.layer.cornerRadius = 3.0
        spinner.clipsToBounds = true
        spinner.hidesWhenStopped = true
        spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge;
        spinner.center = view.center
        DispatchQueue.main.async {
            self.view.addSubview(spinner)
            spinner.startAnimating()
        }
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    /*
     This method hides the Spinner View
     */
    func hideLoader() {
        DispatchQueue.main.async {
            let spinner = self.view.subviews.filter({ $0 is UIActivityIndicatorView }).first as? UIActivityIndicatorView
            spinner?.stopAnimating()
            spinner?.removeFromSuperview()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
    
    /*
     This method is used to present an Alert Controller with Single OK Button
     */
    func presentAlert(with title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
