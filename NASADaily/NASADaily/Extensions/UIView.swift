//
//  UIView.swift
//  NASADaily
//
//  Created by jay on 2018-04-12.
//

import UIKit

extension UIView {
    
    /*
     This method is used to show fadeIn animation with default time Interval
     */
    func fadeIn(withDuration duration: TimeInterval = 0.35) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    /*
     This method is used to show fadeOut animation with default time Interval
     */
    func fadeOut(withDuration duration: TimeInterval = 0.35) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
}
